/*****************************************************************************\
 *  select_nvidia_sharp.c - node selection plugin for clusters with NVIDIA
 *  Scalable Hierarchical Aggregation and Reduction Protocol (SHARP)
 *****************************************************************************
 *  Copyright (C) 2021-2023 NVIDIA CORPORATION. All rights reserved.
 *  Written by NVIDIA CORPORATION.
 *
 *  This file is part of Slurm, a resource management program.
 *  For details, see <https://slurm.schedmd.com/>.
 *  Please also read the included file: DISCLAIMER.
 *
 *  Slurm is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version.
 *
 *  In addition, as a special exception, the copyright holders give permission
 *  to link the code of portions of this program with the OpenSSL library under
 *  certain conditions as described in each individual source file, and
 *  distribute linked combinations including the two. You must obey the GNU
 *  General Public License in all respects for all of the code used other than
 *  OpenSSL. If you modify file(s) with this exception, you may extend this
 *  exception to your version of the file(s), but you are not obligated to do
 *  so. If you do not wish to do so, delete this exception statement from your
 *  version.  If you delete this exception statement from all source files in
 *  the program, then also delete it here.
 *
 *  Slurm is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with Slurm; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA.
\*****************************************************************************/

#include "src/common/slurm_xlator.h"
#include "src/common/xstring.h"
#include "src/common/list.h"
#include "src/interfaces/topology.h"
#include "src/interfaces/select.h"
#include "src/common/pack.h"

#include "src/slurmctld/licenses.h"

#include "other_select.h"

#define JOBINFO_MAGIC 0x8b5d

struct select_jobinfo {
	uint16_t magic;
	char *network;
	select_jobinfo_t *other;
};

const char plugin_name[]	= "NVIDIA SHARP node selection wrapper";
const char plugin_type[]	= "select/nvidia_sharp";
uint32_t plugin_id		= SELECT_PLUGIN_NVIDIA_SHARP;
const uint32_t plugin_version	= SLURM_VERSION_NUMBER;

static dynamic_plugin_data_t *topoinfo = NULL;
static topoinfo_tree_t *topoinfo_tree = NULL;

list_t *cluster_license_list;

/* Hardcode mode to SHARP v3 as we had to remove CR_NVIDIA_SHARP_V3 in 23.11 */
static uint32_t nvidia_sharp_version = 3;

static bool reclaim_sharp_licenses = false;

/* topoinfo_switch_t doesn't container the node bitmap, we need to rebuild it. */
typedef struct {
	char *name;
	char *nodes;
	uint16_t level;
	bitstr_t *node_bitmap;
} switch_info_t;

static list_t *switch_list = NULL;

/* Taken from src/plugins/topology/tree/switch_record.c, copyright SchedMD LLC. */
static int _node_name2bitmap(const char *node_names, bitstr_t **bitmap,
			     hostlist_t **invalid_hostlist)
{
	char *this_node_name;
	bitstr_t *my_bitmap;
	hostlist_t *host_list;

	my_bitmap = (bitstr_t *) bit_alloc(node_record_count);
	*bitmap = my_bitmap;

	if (node_names == NULL) {
		error("_node_name2bitmap: node_names is NULL");
		return EINVAL;
	}

	if ((host_list = hostlist_create(node_names)) == NULL) {
		/* likely a badly formatted hostlist */
		error("_node_name2bitmap: hostlist_create(%s) error",
		      node_names);
		return EINVAL;
	}

	while ((this_node_name = hostlist_shift(host_list)) ) {
		node_record_t *node_ptr;
		node_ptr = find_node_record(this_node_name);
		if (node_ptr) {
			bit_set(my_bitmap, node_ptr->index);
		} else {
			debug2("_node_name2bitmap: invalid node specified %s",
			       this_node_name);
			if (*invalid_hostlist) {
				hostlist_push_host(*invalid_hostlist,
						   this_node_name);
			} else {
				*invalid_hostlist =
					hostlist_create(this_node_name);
			}
		}
		free(this_node_name);
	}
	hostlist_destroy(host_list);

	return SLURM_SUCCESS;
}

static void switch_info_destroy(switch_info_t *sw)
{
	if (!sw)
		return;

	xfree(sw->name);
	xfree(sw->nodes);
	FREE_NULL_BITMAP(sw->node_bitmap);
	xfree(sw);
}

static int cmp_switch(void *x, void *key)
{
	switch_info_t *sw = x;
	return !xstrcmp(sw->name, key);
}

static int nvidia_sharp_topology_init(void)
{
	int rc;

	rc = topology_g_init();
	if (rc != SLURM_SUCCESS)
		return rc;

	rc = topology_g_topology_free(topoinfo);
	topoinfo = NULL;
	topoinfo_tree = NULL;
	if (rc != SLURM_SUCCESS)
		return rc;

	rc = topology_g_get(TOPO_DATA_TOPOLOGY_PTR, &topoinfo);
	if (rc != SLURM_SUCCESS)
		return rc;

	if (topoinfo == NULL || topoinfo->plugin_id != TOPOLOGY_PLUGIN_TREE) {
		error("only topology/tree is supported for select/nvidia_sharp");
		return SLURM_ERROR;
	}
	topoinfo_tree = (topoinfo_tree_t*)topoinfo->data;

	FREE_NULL_LIST(switch_list);
	switch_list = list_create((ListDelF)switch_info_destroy);
	for (int i = 0; i < topoinfo_tree->record_count; ++i) {
		hostlist_t *invalid_hl = NULL;
		switch_info_t *sw = xmalloc(sizeof(*sw));

		sw->name = xstrdup(topoinfo_tree->topo_array[i].name);
		sw->nodes = xstrdup(topoinfo_tree->topo_array[i].nodes);
		sw->level = topoinfo_tree->topo_array[i].level;

		rc = _node_name2bitmap(sw->nodes, &sw->node_bitmap, &invalid_hl);
		if (rc != SLURM_SUCCESS) {
			switch_info_destroy(sw);
			return rc;
		}
		if (invalid_hl != NULL) {
			warning("Switch \"%s\" has invalid hostnames", sw->name);
			hostlist_destroy(invalid_hl);
		}

		list_append(switch_list, sw);
	}

	return SLURM_SUCCESS;
}

static int nvidia_sharp_init(void)
{
	if (!running_in_slurmctld())
		return SLURM_SUCCESS;

	other_select_type_param = slurm_conf.select_type_param;

	if (other_select_type_param & CR_NVIDIA_SHARP_V3)
		nvidia_sharp_version = 3;
	else
		nvidia_sharp_version = 2;

	info("using SHARP v%d", nvidia_sharp_version);

	reclaim_sharp_licenses = false;
	if (xstrcasestr(slurm_conf.preempt_params, "reclaim_licenses"))
		reclaim_sharp_licenses = true;

	return nvidia_sharp_topology_init();
}

extern int init(void)
{
	int rc;

	rc = nvidia_sharp_init();
	if (rc != SLURM_SUCCESS)
		return rc;

	return other_select_init();
}

extern int fini(void)
{
	FREE_NULL_LIST(switch_list);

	topology_g_topology_free(topoinfo);
	topoinfo = NULL;

	return other_select_fini();
}

extern int select_p_state_save(char *dir_name)
{
	return other_state_save(dir_name);
}

extern int select_p_state_restore(char *dir_name)
{
	return other_state_restore(dir_name);
}

extern int select_p_job_init(list_t *job_list)
{
	return other_job_init(job_list);
}

extern int select_p_node_init(void)
{
	return other_node_init();
}

static void free_job_licenses(job_record_t *job_ptr)
{
	FREE_NULL_LIST(job_ptr->licenses_to_preempt);
	FREE_NULL_LIST(job_ptr->license_list);
	xfree(job_ptr->licenses);
}

static bool has_switch_licenses(const job_record_t *job_ptr)
{
	list_itr_t *iter;
	licenses_t *license_entry;
	bool found = false;

	if (job_ptr->license_list == NULL)
		return false;

	iter = list_iterator_create(job_ptr->license_list);
	while ((license_entry = list_next(iter))) {
		if (list_find_first(switch_list, cmp_switch, license_entry->name)) {
			found = true;
			break;
		}
	}
	list_iterator_destroy(iter);

	return found;
}

static void remove_switch_licenses(job_record_t *job_ptr)
{
	list_itr_t *iter;
	licenses_t *license_entry;
	bool modified = false;

	if (job_ptr->license_list == NULL)
		return;

	iter = list_iterator_create(job_ptr->license_list);
	while ((license_entry = list_next(iter))) {
		if (list_find_first(switch_list, cmp_switch, license_entry->name)) {
			modified = true;
			list_delete_item(iter);
		}
	}
	list_iterator_destroy(iter);

	if (list_is_empty(job_ptr->license_list)) {
		free_job_licenses(job_ptr);
		return;
	}

	if (modified) {
		xfree(job_ptr->licenses);
		job_ptr->licenses = license_list_to_string(job_ptr->license_list);
	}
}

static int sharp_filter_nodes(job_record_t *job_ptr, bitstr_t *node_bitmap,
			      uint32_t max_nodes, uint16_t mode, list_t *license_list)
{
	int rc = SLURM_ERROR;
	char *saved_job_licenses = NULL;
	list_itr_t *iter = NULL;
	switch_info_t *switch_ptr;
	list_t *switch_license_list = NULL;
	bool license_valid = false;

	if (topoinfo_tree->record_count == 0 || topoinfo_tree->topo_array == NULL) {
		error("no switch information found, is topology/tree enabled?");
		return SLURM_ERROR;
	}

	/* Temporarily remove non-SHARP licenses to have the rest of the function only deal with SHARP licenses */
	if (job_ptr->licenses != NULL) {
		saved_job_licenses = job_ptr->licenses;
		job_ptr->licenses = NULL;
	}
	free_job_licenses(job_ptr);

	verbose("%pJ: %d nodes before filtering", job_ptr, bit_set_count(node_bitmap));

	iter = list_iterator_create(switch_list);
	while ((switch_ptr = list_next(iter))) {
		/*
		 * Only filter allocated leaf switches here, since they definitely can't be reused
		 * in SHARP v2. Non-leaf switches might not be required, so additional filtering is
		 * performed in job_allocate_switches.
		 */
		if (switch_ptr->level != 0)
			continue;

		if (bit_overlap(node_bitmap, switch_ptr->node_bitmap) == 0)
			continue;

		switch_license_list = license_validate(switch_ptr->name, true, true, NULL, &license_valid);
		if (!license_valid) {
			error("invalid switch license: %s", switch_ptr->name);
			goto end;
		}

		/* Temporarily add a license to the job, to be able to call license_job_test. */
		job_ptr->license_list = switch_license_list;

		rc = license_job_test_with_list(job_ptr, time(NULL), true, license_list);
		free_job_licenses(job_ptr);

		/* Leaf switch has available resources */
		if (rc == SLURM_SUCCESS)
			continue;

		if (rc != EAGAIN) {
			error("sharp_filter_nodes: invalid licenses state");
			goto end;
		}

		if (nvidia_sharp_version == 3) {
			/*
			 * If the underlying select plugin can place the job on a single switch, a switch license will
			 * not be needed under SHARP v3. We should only filter switches that could not possibly fit the
			 * job, as a cross-leaf license would certainly be needed if using nodes from this switch. If a
			 * single switch would have worked but nodes end up being allocated across multiple switches
			 * for any reason, the node selection will be rejected later in job_allocate_switches.
			 */
			if (mode == SELECT_MODE_RUN_NOW &&
			    bit_overlap(node_bitmap, switch_ptr->node_bitmap) >= max_nodes) {
				continue;
			}
		}

		/*
		 * This leaf switch has no more licenses available and the job cannot be a "free" SHARP v3 job,
		 * therefore we remove the nodes under this switch
		 */
		verbose("%pJ: excluding nodes under switch %s", job_ptr, switch_ptr->name);
		bit_and_not(node_bitmap, switch_ptr->node_bitmap);
	}

	verbose("%pJ: %d nodes after filtering", job_ptr, bit_set_count(node_bitmap));

	rc = SLURM_SUCCESS;

end:
	list_iterator_destroy(iter);

	/* Restore non-SHARP licenses */
	if (saved_job_licenses != NULL) {
		job_ptr->licenses = saved_job_licenses;
		saved_job_licenses = NULL;
	}
	license_job_merge(job_ptr);

	return rc;
}

static int job_allocate_switches(job_record_t *job_ptr, bitstr_t *node_bitmap, list_t *license_list)
{
	int rc;
	list_itr_t *iter = NULL;
	switch_info_t *switch_ptr, *root_switch;
	char *switch_licenses = NULL;
	list_t *switch_license_list = NULL;
	bool license_valid = false;

	if (node_bitmap == NULL || bit_set_count(node_bitmap) == 0) {
		error("%pJ: no nodes assigned to job", job_ptr);
		return SLURM_ERROR;
	}

	if (topoinfo_tree->record_count == 0 || topoinfo_tree->topo_array == NULL) {
		error("no switch information found, is topology/tree enabled?");
		return SLURM_ERROR;
	}

	/* Find the root switch of the tree: the *lowest* level switch containing all allocated nodes */
	root_switch = NULL;
	iter = list_iterator_create(switch_list);
	while ((switch_ptr = list_next(iter))) {
		if (!bit_super_set(node_bitmap, switch_ptr->node_bitmap))
			continue;

		if ((root_switch == NULL) ||
		    (switch_ptr->level < root_switch->level)) {
			root_switch = switch_ptr;
		}
	}
	list_iterator_destroy(iter);
	if (root_switch == NULL) {
		error("could not find root switch for job, make sure one switch can reach all nodes");
		return SLURM_ERROR;
	}
	verbose("%pJ: root switch: %s (level: %d)", job_ptr, root_switch->name, root_switch->level);

	if (nvidia_sharp_version == 3 && root_switch->level == 0) {
		verbose("%pJ: all nodes are under one leaf switch, no license allocation needed", job_ptr);
		return SLURM_SUCCESS;
	}

	iter = list_iterator_create(switch_list);
	while ((switch_ptr = list_next(iter))) {
		/* Skip switches above the root switch of the job, they don't need to be allocated */
		if (switch_ptr->level > root_switch->level)
			continue;

		if (bit_overlap_any(node_bitmap, switch_ptr->node_bitmap))
			xstrfmtcat(switch_licenses, "%s%s:1", (switch_licenses ? "," : ""), switch_ptr->name);
	}
	list_iterator_destroy(iter);

	verbose("%pJ: required switch licenses: %s", job_ptr, switch_licenses);

	switch_license_list = license_validate(switch_licenses, true, true, NULL, &license_valid);
	if (!license_valid) {
		error("%pJ: job_allocate_switches: invalid licenses list: %s", job_ptr, switch_licenses);
		xfree(switch_licenses);
		return SLURM_ERROR;
	}
	FREE_NULL_LIST(switch_license_list);

	if (job_ptr->licenses == NULL) {
		job_ptr->licenses = switch_licenses;
		switch_licenses = NULL;
	} else {
		xstrfmtcat(job_ptr->licenses, ",%s", switch_licenses);
	}
	license_job_merge(job_ptr);
	xfree(switch_licenses);

	rc = license_job_test_with_list(job_ptr, time(NULL), true, license_list);
	if (rc != SLURM_SUCCESS) {
		if (rc == EAGAIN)
			verbose("%pJ: switches are not available right now", job_ptr);
		else
			error("%pJ: job_allocate_switches: invalid licenses state", job_ptr);
		remove_switch_licenses(job_ptr);
		return rc;
	}

	return SLURM_SUCCESS;

}

static int select_sharp_will_run(job_record_t *job_ptr, bitstr_t *node_bitmap,
				 uint32_t min_nodes, uint32_t max_nodes,
				 uint32_t req_nodes,
				 list_t *preemptee_candidates,
				 list_t **preemptee_job_list,
				 resv_exc_t *resv_exc_ptr,
				 will_run_data_t *will_run_ptr,
				 list_t *license_list)
{
	int rc;

	/*
	 * If mode == SELECT_MODE_WILL_RUN, we must still verify SHARP resources,
	 * otherwise Slurm can get confused if MODE_WILL_RUN succeeds but MODE_RUN_NOW
	 * fails (because of the additional nodes filtering). The Slurm backfill
	 * algorithm would then allocate an "internal reservation" for the SHARP jobs,
	 * working under the assumption that these jobs should be able to start soon:
	 * https://github.com/SchedMD/slurm/blob/slurm-23-02-5-1/src/plugins/sched/backfill/backfill.c#L2818-L2826
	 * This can potentially prevent non-SHARP jobs from being scheduled at all.
	 */
	rc = sharp_filter_nodes(job_ptr, node_bitmap, max_nodes,
				SELECT_MODE_WILL_RUN, license_list);
	if (rc != SLURM_SUCCESS)
		goto end;

	rc = other_job_test(job_ptr, node_bitmap, min_nodes, max_nodes,
			    req_nodes, SELECT_MODE_WILL_RUN, preemptee_candidates,
			    preemptee_job_list, resv_exc_ptr, will_run_ptr);
	if (rc != SLURM_SUCCESS) {
		verbose("%pJ: other_job_test error: %d", job_ptr, rc);
		rc = ESLURM_ACCOUNTING_POLICY;
		goto end;
	}

	rc = job_allocate_switches(job_ptr, node_bitmap, license_list);
	if (rc != SLURM_SUCCESS) {
		verbose("%pJ: job_allocate_switches failed", job_ptr);
		free_job_resources(&job_ptr->job_resrcs);
		goto end;
	}

	verbose("%pJ: mode=will_run, freeing job licenses and resources", job_ptr);
	remove_switch_licenses(job_ptr);
	free_job_resources(&job_ptr->job_resrcs);

	rc = SLURM_SUCCESS;

end:
	return rc;
}

static int select_sharp_run_now(job_record_t *job_ptr, bitstr_t *node_bitmap,
				uint32_t min_nodes, uint32_t max_nodes,
				uint32_t req_nodes,
				list_t *preemptee_candidates,
				list_t **preemptee_job_list,
				resv_exc_t *resv_exc_ptr)
{
	int rc;
	bitstr_t *initial_node_bitmap = bit_copy(node_bitmap);
	list_t *future_license_list = license_copy(cluster_license_list);
	job_record_t *tmp_job_ptr;
	list_itr_t *preemptee_iterator;

	rc = sharp_filter_nodes(job_ptr, node_bitmap, max_nodes,
				SELECT_MODE_RUN_NOW, cluster_license_list);
	if (rc != SLURM_SUCCESS)
		goto end;

	rc = other_job_test(job_ptr, node_bitmap, min_nodes, max_nodes,
			    req_nodes, SELECT_MODE_RUN_NOW, preemptee_candidates,
			    preemptee_job_list, resv_exc_ptr, NULL);
	if (rc != SLURM_SUCCESS) {
		if (!reclaim_sharp_licenses || list_count(preemptee_candidates) == 0 ||
		    preemptee_job_list == NULL) {
			verbose("%pJ: other_job_test failed and license reclaim not possible", job_ptr);
			rc = ESLURM_ACCOUNTING_POLICY;
			goto end;
		}

		bit_copybits(node_bitmap, initial_node_bitmap);

		/* Test with all preemptible SHARP jobs removed */
		preemptee_iterator = list_iterator_create(preemptee_candidates);
		while ((tmp_job_ptr = list_next(preemptee_iterator))) {
			if (has_switch_licenses(tmp_job_ptr) && tmp_job_ptr->node_bitmap != NULL) {
				bit_or(node_bitmap, tmp_job_ptr->node_bitmap);
				license_job_return_to_list(tmp_job_ptr, future_license_list);
			}
		}
		list_iterator_destroy(preemptee_iterator);

		rc = select_sharp_will_run(job_ptr, node_bitmap, min_nodes, max_nodes,
					   req_nodes, NULL, NULL, resv_exc_ptr, NULL,
					   future_license_list);
		if (rc != SLURM_SUCCESS) {
			verbose("%pJ: switch licenses reclaim was unsuccessful: %d", job_ptr, rc);
			rc = ESLURM_ACCOUNTING_POLICY;
			goto end;
		}

		if (*preemptee_job_list == NULL)
			*preemptee_job_list = list_create(NULL);

		preemptee_iterator = list_iterator_create(preemptee_candidates);
		while ((tmp_job_ptr = list_next(preemptee_iterator))) {
			/* Build list of preemptee jobs whose resources are actually used. */
			if (!bit_overlap_any(node_bitmap, tmp_job_ptr->node_bitmap))
				continue;
			list_append(*preemptee_job_list, tmp_job_ptr);
		}
		list_iterator_destroy(preemptee_iterator);
	}

	rc = job_allocate_switches(job_ptr, node_bitmap, future_license_list);
	if (rc != SLURM_SUCCESS) {
		verbose("%pJ: job_allocate_switches failed", job_ptr);
		free_job_resources(&job_ptr->job_resrcs);
		goto end;
	}

	if (job_ptr->job_resrcs == NULL) {
		/* We need to preempt jobs before allocating switch licenses. */
		verbose("%pJ: job preemption required, removing switch licenses", job_ptr);
		remove_switch_licenses(job_ptr);
	}

	rc = SLURM_SUCCESS;

end:
	FREE_NULL_BITMAP(initial_node_bitmap);
	FREE_NULL_LIST(future_license_list);
	return rc;
}

extern int select_p_job_test(job_record_t *job_ptr, bitstr_t *node_bitmap,
			     uint32_t min_nodes, uint32_t max_nodes,
			     uint32_t req_nodes, uint16_t mode,
			     list_t *preemptee_candidates,
			     list_t **preemptee_job_list,
			     resv_exc_t *resv_exc_ptr,
			     will_run_data_t *will_run_ptr)
{
	int rc;
	char *network = NULL;

	rc = select_g_select_jobinfo_get(job_ptr->select_jobinfo, SELECT_JOBDATA_NETWORK, &network);
	if (rc != SLURM_SUCCESS) {
		verbose("couldn't get network jobinfo");
		network = NULL;
	}

	if (network == NULL || strcmp(network, "sharp") != 0) {
		return other_job_test(job_ptr, node_bitmap, min_nodes, max_nodes,
				      req_nodes, mode, preemptee_candidates,
				      preemptee_job_list, resv_exc_ptr, will_run_ptr);
	}

	verbose("%pJ: mode=%d, part=%s", job_ptr, mode,
		job_ptr->part_ptr ? job_ptr->part_ptr->name : "(null)");

	/* Check --exclusive on the job allocation. */
	if (!(job_ptr->details->whole_node & WHOLE_NODE_REQUIRED)) {
		/* Check OverSubscribe=EXCLUSIVE on the partition. */
		if (job_ptr->part_ptr != NULL && job_ptr->part_ptr->max_share != 0) {
			error("select_p_job_test: %pJ: need whole node allocation for SHARP", job_ptr);
			return SLURM_ERROR;
		}
	}

	if ((job_ptr->licenses != NULL && job_ptr->license_list == NULL) ||
	    (job_ptr->licenses == NULL && job_ptr->license_list != NULL)) {
		error("select_p_job_test: %pJ: invalid job licenses state", job_ptr);
		return SLURM_ERROR;
	}

	/*
	 * With job arrays, the job record for the meta-job also serves as the job record for the
	 * first job in the array. New jobs are cloned from a job that already has SHARP licenses:
	 * https://github.com/SchedMD/slurm/blob/slurm-20-11-7-1/src/slurmctld/job_mgr.c#L4626-L4627
	 * We clear the existing switch licenses in all cases. But job arrays will likely be
	 * scheduled sequentially as they will keep their original license assignment for a while.
	 */
	remove_switch_licenses(job_ptr);

	if (mode == SELECT_MODE_TEST_ONLY) {
		return other_job_test(job_ptr, node_bitmap, min_nodes, max_nodes,
				      req_nodes, mode, preemptee_candidates,
				      preemptee_job_list, resv_exc_ptr, will_run_ptr);
	}

	if (mode == SELECT_MODE_WILL_RUN) {
		rc = select_sharp_will_run(job_ptr, node_bitmap, min_nodes, max_nodes,
					   req_nodes, preemptee_candidates,
					   preemptee_job_list, resv_exc_ptr, will_run_ptr,
					   cluster_license_list);
	} else if (mode == SELECT_MODE_RUN_NOW) {
		rc = select_sharp_run_now(job_ptr, node_bitmap, min_nodes, max_nodes,
					  req_nodes, preemptee_candidates,
					  preemptee_job_list, resv_exc_ptr);
	} else {
		error("%pJ: mode=%d is not supported", job_ptr, mode);
		rc = SLURM_ERROR;
	}

	return rc;
}

extern int select_p_job_begin(job_record_t *job_ptr)
{
	int rc;
	char *network = NULL;

	rc = select_g_select_jobinfo_get(job_ptr->select_jobinfo, SELECT_JOBDATA_NETWORK, &network);
	if (rc != SLURM_SUCCESS)
		network = NULL;

	if (network == NULL || strcmp(network, "sharp") != 0)
		return other_job_begin(job_ptr);

	/* SHARP v2 jobs always require switch licenses, but SHARP v3 jobs do not. */
	if (nvidia_sharp_version == 3 && job_ptr->license_list == NULL)
	       return other_job_begin(job_ptr);

	/*
	 * Switch licenses (for SHARP) are already checked in select_p_job_test,
	 * but we do it again here, to be safe.
	 */
	if (job_ptr->license_list == NULL || license_job_test(job_ptr, time(NULL), true) != SLURM_SUCCESS) {
		error("select_p_job_begin: %pJ: invalid licenses state", job_ptr);
		remove_switch_licenses(job_ptr);
		free_job_resources(&job_ptr->job_resrcs);
		errno = EBUSY;
		return SLURM_ERROR;
	}

	return other_job_begin(job_ptr);
}

extern int select_p_job_ready(job_record_t *job_ptr)
{
	return other_job_ready(job_ptr);
}

extern int select_p_job_resized(job_record_t *job_ptr, node_record_t *node_ptr)
{
	return other_job_resized(job_ptr, node_ptr);
}

extern int select_p_job_expand(job_record_t *from_job_ptr,
			       job_record_t *to_job_ptr)
{
	return other_job_expand(from_job_ptr, to_job_ptr);
}

extern int select_p_job_fini(job_record_t *job_ptr)
{
	return other_job_fini(job_ptr);
}

extern int select_p_job_suspend(job_record_t *job_ptr, bool indf_susp)
{
	return other_job_suspend(job_ptr, indf_susp);
}

extern int select_p_job_resume(job_record_t *job_ptr, bool indf_susp)
{
	return other_job_resume(job_ptr, indf_susp);
}

extern bitstr_t *select_p_step_pick_nodes(job_record_t *job_ptr,
					  select_jobinfo_t *jobinfo,
					  uint32_t node_count,
					  bitstr_t **avail_nodes)
{
	return other_step_pick_nodes(job_ptr, jobinfo, node_count, avail_nodes);
}

extern int select_p_step_start(step_record_t *step_ptr)
{
	return other_step_start(step_ptr);
}

extern int select_p_step_finish(step_record_t *step_ptr, bool killing_step)
{
	return other_step_finish(step_ptr, killing_step);
}

extern select_nodeinfo_t *select_p_select_nodeinfo_alloc(void)
{
	return other_select_nodeinfo_alloc();
}

extern int select_p_select_nodeinfo_free(select_nodeinfo_t *nodeinfo)
{
	return other_select_nodeinfo_free(nodeinfo);
}

extern int select_p_select_nodeinfo_pack(select_nodeinfo_t *nodeinfo,
					 buf_t *buffer, uint16_t protocol_version)
{
	return other_select_nodeinfo_pack(nodeinfo, buffer, protocol_version);
}

extern int select_p_select_nodeinfo_unpack(select_nodeinfo_t **nodeinfo,
					   buf_t *buffer,
					   uint16_t protocol_version)
{
	return other_select_nodeinfo_unpack(nodeinfo, buffer, protocol_version);
}

extern int select_p_select_nodeinfo_set_all(void)
{
	return other_select_nodeinfo_set_all();
}

extern int select_p_select_nodeinfo_set(job_record_t *job_ptr)
{
	return other_select_nodeinfo_set(job_ptr);
}

extern int select_p_select_nodeinfo_get(select_nodeinfo_t *nodeinfo,
					enum select_nodedata_type dinfo,
					enum node_states state,
					void *data)
{
	return other_select_nodeinfo_get(nodeinfo, dinfo, state, data);
}

extern select_jobinfo_t *select_p_select_jobinfo_alloc(void)
{
	select_jobinfo_t *jobinfo = xcalloc(1, sizeof(select_jobinfo_t));

	jobinfo->magic = JOBINFO_MAGIC;
	jobinfo->other = other_select_jobinfo_alloc();

	return jobinfo;
}

extern int select_p_select_jobinfo_set(select_jobinfo_t *jobinfo,
				       enum select_jobdata_type data_type,
				       void *data)
{
	if (jobinfo == NULL || data == NULL)
		return SLURM_ERROR;

	if (jobinfo->magic != JOBINFO_MAGIC) {
		error("jobinfo_set: invalid magic value");
		return SLURM_ERROR;
	}

	if (data_type == SELECT_JOBDATA_NETWORK) {
		jobinfo->network = xstrdup((char*)data);
		return SLURM_SUCCESS;
	}

	return select_p_select_jobinfo_set(jobinfo->other, data_type, data);
}

extern int select_p_select_jobinfo_get(select_jobinfo_t *jobinfo,
				       enum select_jobdata_type data_type,
				       void *data)
{
	char **ptr;

	if (jobinfo == NULL || data == NULL)
		return SLURM_ERROR;

	if (jobinfo->magic != JOBINFO_MAGIC) {
		error("jobinfo_get: invalid magic value");
		return SLURM_ERROR;
	}

	if (data_type == SELECT_JOBDATA_NETWORK) {
		ptr = (char **)data;

		if (jobinfo->network == NULL) {
			*ptr = NULL;
			return SLURM_SUCCESS;
		}

		*ptr = jobinfo->network;

		return SLURM_SUCCESS;
	}

	return other_select_jobinfo_get(jobinfo->other, data_type, data);
}

extern int select_p_select_jobinfo_free(select_jobinfo_t *jobinfo)
{
	int rc = SLURM_SUCCESS;

	if (jobinfo == NULL)
		return SLURM_SUCCESS;

	if (jobinfo->magic != JOBINFO_MAGIC) {
		error("jobinfo_free: invalid magic value");
		return SLURM_ERROR;
	}
	jobinfo->magic = 0;

	xfree(jobinfo->network);
	rc = other_select_jobinfo_free(jobinfo->other);
	xfree(jobinfo);

	return rc;
}

extern select_jobinfo_t *select_p_select_jobinfo_copy(select_jobinfo_t *jobinfo)
{
	select_jobinfo_t *copy;

	if (jobinfo == NULL)
		return NULL;

	if (jobinfo->magic != JOBINFO_MAGIC) {
		error("jobinfo_copy: invalid magic value");
		return NULL;
	}

	copy = select_p_select_jobinfo_alloc();
	if (copy == NULL)
		return NULL;

	if (jobinfo->network != NULL)
		copy->network = xstrdup(jobinfo->network);

	if (jobinfo->other != NULL) {
		copy->other = other_select_jobinfo_copy(jobinfo->other);
		if (copy->other == NULL) {
			select_p_select_jobinfo_free(copy);
			return NULL;
		}
	}

	return copy;
}

extern int select_p_select_jobinfo_pack(select_jobinfo_t *jobinfo,
					buf_t *buffer, uint16_t protocol_version)
{
	if (protocol_version < SLURM_MIN_PROTOCOL_VERSION)
		return SLURM_SUCCESS;

	if (jobinfo == NULL) {
		packstr("", buffer);
		return other_select_jobinfo_pack(NULL, buffer, protocol_version);
	}

	if (jobinfo->magic != JOBINFO_MAGIC) {
		error("jobinfo_pack: invalid magic value");
		return SLURM_ERROR;
	}

	packstr(jobinfo->network, buffer);

	return other_select_jobinfo_pack(jobinfo->other, buffer, protocol_version);
}

extern int select_p_select_jobinfo_unpack(select_jobinfo_t **jobinfo_pptr,
					  buf_t *buffer, uint16_t protocol_version)
{
	int rc;
	select_jobinfo_t *jobinfo = NULL;
	uint32_t uint32_tmp;

	jobinfo = select_p_select_jobinfo_alloc();
	if (jobinfo == NULL)
		return SLURM_ERROR;

	*jobinfo_pptr = jobinfo;
	if (protocol_version < SLURM_MIN_PROTOCOL_VERSION)
		return SLURM_SUCCESS;

	safe_unpackstr_xmalloc(&jobinfo->network, &uint32_tmp, buffer);

	rc = other_select_jobinfo_unpack(&jobinfo->other, buffer, protocol_version);
	if (rc != SLURM_SUCCESS)
		goto unpack_error;

	return SLURM_SUCCESS;

unpack_error:
	error("error unpacking jobinfo");
	select_p_select_jobinfo_free(jobinfo);
	*jobinfo_pptr = NULL;

	return SLURM_ERROR;
}

extern int select_p_get_info_from_plugin(enum select_plugindata_info dinfo,
					 job_record_t *job_ptr,
					 void *data)
{
	return other_get_info_from_plugin(dinfo, job_ptr, data);
}

extern int select_p_reconfigure(void)
{
	int rc;

	rc = nvidia_sharp_init();
	if (rc != SLURM_SUCCESS)
		return rc;

	return other_reconfigure();
}
