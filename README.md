# NVIDIA SHARP select plugin

## Setup
Use the following config to wrap `select/cons_tres`:
```
SelectType=select/nvidia_sharp
SelectTypeParameters=CR_Core,OTHER_CONS_TRES
```

You also need `topology/tree` and need to declare one license per leaf switch, core/spine switches can have more than one license:
```
TopologyPlugin=topology/tree
Licenses=leaf01:1,leaf02:1,leaf03:1,leaf04:1,quad1:10,quad2:10,cluster:7
```
The name of the license must match the name of the switch in `topology.conf`. The licenses should not be requested
by users directly.

## Usage
```
$ srun --network sharp -N2 ...
```

## Assumptions
We assume that all nodes are allocated exclusively, hence we don't need any special logic for handling multi-rail: all
planes are equivalent and allocated similarly.

## Implementation
The plugin implements a Slurm [Resource Selection Plugin](https://slurm.schedmd.com/selectplugins.html) that can wrap a
builtin Slurm node selection plugin like `select/cons_tres`. This is the same approach than the `cray_aries` plugin. By
using a wrapper, we don't need to modify the core Slurm plugins, thus we can go back to `select/cons_tres` by just
editing `slurm.conf`, no need to rebuild Slurm without this patch.

This plugin is activated when `--network sharp` is used in the job submission, this is achieved by implementing the
`select_p_select_jobinfo_*` API functions in our wrapper.

The bulk of the logic is in the API function `select_p_job_test`:
* If `--network sharp` was not specified, we passthrough to the wrapped plugin.
* If the `mode` argument is *not* `SELECT_MODE_RUN_NOW`, we also passthrough to the wrapped plugin.
* We filter the list of available nodes (`bitmap` argument) to exclude nodes which are currently under a fully-allocated
  switch.
* We call the wrapped plugin with this updated node list.
* If the wrapped plugin is successful in finding a node allocation with the available switches, we prepare the switch
  allocation by adding the corresponding switch licenses to the job. We need to find the root switch, i.e. the *lowest*
  level switch containing all the allocated nodes. We then require one license for each switch below the root switch.
* We do not acquire the licenses in the plugin, this is done in the core of Slurm, by the `allocate_nodes()` function.
* In `select_p_job_begin`, we check again that the switch licenses are still available for our job. This is a sanity
  check, it should not be failing at this point.

## Troubleshooting
You can't request a license at the same time, the job will be rejected immediately:
```
srun -N1 --network sharp --license foo:1
srun: error: Unable to allocate resources: Requested node configuration is not available
```

You can check which switch licenses a job is using:
```
$ scontrol show job 321 | grep License
   OverSubscribe=NO Contiguous=0 Licenses=leaf04:1,leaf01:1 Network=sharp
```

To check the status of all switches:
```
$ scontrol show license
LicenseName=leaf01
    Total=1 Used=1 Free=0 Remote=no
LicenseName=leaf02
    Total=1 Used=0 Free=1 Remote=no
LicenseName=leaf03
    Total=1 Used=0 Free=1 Remote=no
LicenseName=leaf04
    Total=1 Used=1 Free=0 Remote=no
```

## TODO
* Do not allocate switches that are not used in aggregation mode (only a single child).
